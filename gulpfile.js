var gulp = require('gulp');

var fileinclude = require('gulp-file-include');
gulp.task('fileinclude', function() {
  gulp.src(['./src/*.html'])
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('dest/'));
});


gulp.task('watch', function () {
  gulp.watch('./src/**/*.html', ['fileinclude']);
});

gulp.task('default', ['fileinclude']);